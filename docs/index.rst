################################
Omniblack Secret's Documentation
################################

.. automodule:: omniblack.secret

    .. py:function:: configure_password_checker(path)

        Use the configuration at "path" for the password checker.
        This should be a valid configuration file for libpasswdqc.
        By default we use :code:`/etc/passwdqc.conf`.

        :param path: The path of the configuration to load.
        :type path: :type:`os.PathLike`

    .. py:function:: is_password_checker_configured()

        Has the libpasswdqc configuration load successfully?

    .. autoclass:: Secret(secret)
        :show-inheritance:

        :param secret: The secret to store.
        :type secret: :type:`str`

        .. py:method:: reveal()

            Return the secret.

            :rtype: :type:`str`

        .. py:method:: random_secret(len)
            :classmethod:

            Return a new random url safe string.

            :param len: The length of the generated string.
            :type len: :type:`int`

            :rtype: :type:`Secret`

        .. py:method:: __rich__()

            Return a rich renderable safely representing the secret.

            :rtype: :type:`rich.abc.RichRenderable`

        .. py:method:: __repr__()

            Return a safe string representation of the secret.

        .. py:method:: __len__()

            Return the length of the secret in unicode codepoints.

        .. py:method:: __copy__()

            Return a new copy of the secret.


        .. py:method:: __deepcopy__()

            Return a new copy of the secret.

    .. autoclass:: Password(password)
        :show-inheritance:

        .. py:method:: verify_password_against(hashedPassword)

            Check if this password matches :code:`hashedPassword`.

            .. IMPORTANT::
                This method should be used instead instead of
                :code:`==` on a string.
                This method utilizes libsodium to perform a cryptographically
                secure comparisons.

            :param hashedPassword: The hashed secret to compare against.
            :type hashedPassword: :type:`omniblack.secret.Secret`

            :returns: Return a boolean indicating if when :code:`self` is hashed
                it matches :code:`other`.
            :rtype: :type:`bool`

        .. py:method:: hash()

            Return the hash of this password.

            :rtype: :type:`Password`

        .. py:method:: need_rehash()

            Check if this hash need to be recreated.

            .. NOTE::
                :class: dropdown

                This always returns true if :code:`self` is not in
                libsodium's stored password format.

        .. py:method:: check_quality()

            Check if the password is strong enough.

            :returns: A string describing the problem or :type:`None`
                if the password is strong enough.

            :rtype: :type:`None | str`

            .. note::
                :class: dropdown

                Only useful on the unhashed password.
                It checks the actual content of the secret
                using libpasswdqc's :code:`passwdqc_check`.

        .. py:method:: random_password()

            Return a new random password.

            .. note::
                :class: dropdown

                This uses libpasswdqc's :code:`passwdqc_random`
                to generate the password.
