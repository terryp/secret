# Configuration file for the Sphinx documentation builder.
from datetime import date
from os.path import dirname as dir_name, exists, join, relpath as relative_path

_today = date.today()

project = 'omniblack.secret'
copyright = f'{_today}, Terry Patterson'
author = 'Terry Patterson'
release = '0.0.10'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.extlinks',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',

    'sphinx_last_updated_by_git',
    'sphinx-prompt',
    'sphinx_copybutton',
    'myst_parser',
    'sphinx_design',
    'sphinx_togglebutton',

    'hoverxref.extension',
    'omniblack.sphinx_type_role',
]

napoleon_preprocess_types = True
napoleon_numpy_docstring = True
napoleon_google_docstring = True

myst_enable_extensions = [
    'colon_fence',
    'deflist',
    'attrs_block',
    'attrs_inline',
    'fieldlist',
]


templates_path = ['_templates']
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
html_use_smartypants = False
smartquotes = False

# -- Options for intersphinx extension ---------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#configuration

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'rich': ('https://rich.readthedocs.io/en/stable/', None),
}
