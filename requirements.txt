pytest
pytest-pretty

sphinx
myst-parser
sphinx-design
sphinx-last-updated-by-git
sphinx-hoverxref
sphinx-prompt
sphinx-copybutton

omniblack.sphinx-type-role

omniblack.setup >= 0.1.1
build ~= 1.2.1
